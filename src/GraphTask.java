import java.io.*;
import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) throws IOException {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() throws IOException {
      //Graph pageGraph = createPageGraph("testData/goodExample.txt");
      //System.out.println(pageGraph);
   }


   /**
    * Reads in a file from a path that is given as a parameter.
    * Returns buffered fail as BufferedReader file type for further processing.
    * @param filePath
    */
   private static BufferedReader bufferCSV(String filePath) throws FileNotFoundException {
      File file = new File(filePath);
      return new BufferedReader(new FileReader(file));
   }

   /**
    * Checks whether the given CSV is correctly formatted and returns an expected row.
    * Throws an error that describes filepath, row and row number of a file, where the error occurred.
    * @param filePath - file's filepath.
    * @param row - the row that is checked in CSV file.
    * @param counter - row's number.
    */
   private static void checkCSV(String row, String filePath, int counter) {
      String[] commaSplit = row.split(",");
      if (commaSplit.length < 2 || (!commaSplit[1].startsWith("\"") && commaSplit.length > 2)) {
         throw new IllegalArgumentException(String.format("" +
                         "\n\nThe row '%s' in file '%s'(row nr %s) is invalid. \nProbably there is a missing" +
                         "comma or missing links list. \nPlease note that" +
                         "even empty list has to be present as an argument.\n",
                 row, filePath, counter));
      }
   }

   /**
    * Creates a stack from a row in CSV file, where the first element in a
    * web page that has links and elements after that are page where the link/ links take(s).
    * Returns buffered fail as BufferedReader file type for further processing.
    * @param row
    */
   private static LinkedList<String> createStack(String row) {
      return new LinkedList<> (Arrays.asList(
              row.replaceAll("\"", "").split(",")));
   }

   /**
    * This methods puts everything together and returns data given in CSV file as a graph.
    * First, the new graph is created using Graph class. Then bufferedCSV method is called
    * to buffer the CSV file using @params filepath.
    * The lines of CSV file are read in a loop. The first row is skipped as a header.
    * Next, the validity of a row is controlled using checkCSV method.
    * If everything is fine, the stack is created for further processing.
    * The origin page (page that has the links) is the first element in a stack and gets
    * created as a Vertex class element indicating a node in a graph. The very next element
    * will be a first link from given origin page and get linked using Arc class, that functions
    * as a directed edge in a graph, and its methods.
    * The same process is repeated in a loop if there are more links to add.
    * The returned value is a graph of pages.
    * @param filePath
    */
   public Graph createPageGraph(String filePath) throws IOException {
      Graph pageLink = new Graph("PageLink");
      BufferedReader br = bufferCSV(filePath);
      String row;
      int counter = 0;

      while ((row = br.readLine()) != null) {
         if (++counter == 1) { continue;}
         checkCSV(row, filePath, counter);

         LinkedList<String> rowStack = createStack(row);
         Vertex origin = pageLink.setVertex(rowStack.pop());
         Vertex target = pageLink.setVertex(rowStack.pop());
         Arc link = pageLink.initializeLink(origin, target);
         origin.setFirst(link);

         if (rowStack.size() > 0) pageLink.setArcs(rowStack, origin, link);
      }
      return pageLink;
   }

   /** Vertex represents a node in a graph, where pages are linked together
    * Vertexes represents a single origin page in a pageLink graph.
    */
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      // You can add more fields, if needed

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      /**
       * getters and setters
       */
      public String getId() { return id; }
      public Vertex getNext() { return next; }
      public void setNext(Vertex next) { this.next = next; }
      public Arc getFirst() { return first; }
      public void setFirst(Arc first) { this.first = first; }
      public int getInfo() { return info; }
      public void setInfo(int info) { this.info = info; }
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      // You can add more fields, if needed

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      /**
       * getters and setters
       */
      public Vertex getTarget() { return target; }
      public void setTarget(Vertex target) { this.target = target; }
      public Arc getNext() { return next; }
      public void setNext(Arc next) { this.next = next; }
   }

   /** Graph is graph data structure that has nodes and edges.
    * In this solution nodes are Vertexes and oriented edges are Arcs.
    * Pages linked together are represented as a graph.
    */
   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      LinkedList<String> pageCache = new LinkedList<>();
      // You can add more fields, if needed

      Graph (String s, Vertex v) {
         id = s;
         first = v;
         LinkedList<String> pageCache = new LinkedList<>();
      }

      Graph (String s) {
         this (s, null);
      }

      /**
       * getters and setters
       */
      public String getId() { return id; }
      public Vertex getFirst() { return first; }
      public void setFirst(Vertex first) { this.first = first; }

      /**
       * Adds a Vertex (page) to the cache.
       * @param vertexName
       */
      public void addToCache(String vertexName) {
         this.pageCache.add(vertexName);
      }

      /**
       * Checks whether the Vertex (page) is already present in a Graph using the cache.
       * @param vertexName
       */
      public boolean pageIsCached(String vertexName) {
         return this.pageCache.contains(vertexName);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      /**
       * Creates a Vertex to a graph.
       * @param vid
       */
      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      /**
       * Creates an Arc from two Vertexes (origin and target) and names it.
       * @param aid - Arc's name
       * @param from - origin Vertex
       * @param to - target Vertex
       */
      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      /**
       * This function searches a Vertex in a graph using it's id.
       * @param id number of vertices added to this graph
       */
      public Vertex getVertexById(String id) {
         Vertex graphVertex = this.first;
         while (graphVertex != null) {
            if (graphVertex.getId().equals(id)) { return graphVertex; }
            graphVertex = graphVertex.getNext();
         }
         return null;
      }

      /**
       * This function sets a Vertex for processing in a graph using Vertex name.
       * If the Vertex already exists in a graph, new Vertex to that graph is created.
       * Otherwise already existing Vertex is used.
       * @param vertexName
       */
      public Vertex setVertex(String vertexName) {
         if (!this.pageIsCached(vertexName))
         {
            this.addToCache(vertexName);
            return this.createVertex(vertexName);
         }
         else { return this.getVertexById(vertexName); }
      }

      /**
       * This function initializes a link between origin Vertex (page) and target Vertex (page).
       * @param origin
       * @param target
       */
      public Arc initializeLink(Vertex origin, Vertex target) {
         Arc link = new Arc("{"+origin + "->" + target+"}");
         link.setTarget(target);
         return link;
      }

      /**
       * This function sets all the Arcs (links) for a given Vertex (page) using
       * the CSV row represented as a rowStack, origin Vertex (page) and the first
       * Arc (link) that belongs to that origin Vertex (page).
       * @param origin
       * @param rowStack
       * @param link
       */
      public void setArcs(LinkedList<String> rowStack, Vertex origin, Arc link) {
         while (rowStack.size() != 0)
         {
            Vertex nextTarget = this.setVertex(rowStack.pop());
            Arc nextLink = this.initializeLink(origin, nextTarget);
            link.setNext(nextLink);
            link = nextLink;
         }
      }
   }

} 

